-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : sam. 15 mai 2021 à 18:06
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `mery_planning_cours`
--

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

DROP TABLE IF EXISTS `etudiant`;
CREATE TABLE IF NOT EXISTS `etudiant` (
  `id_etudiant` int(11) NOT NULL AUTO_INCREMENT,
  `id_utilisateur` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telephone` int(10) NOT NULL,
  `Appogee` int(10) NOT NULL,
  PRIMARY KEY (`id_etudiant`),
  KEY `user_id` (`id_utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `etudiant`
--

INSERT INTO `etudiant` (`id_etudiant`, `id_utilisateur`, `nom`, `prenom`, `email`, `telephone`, `Appogee`) VALUES
(1, 1, 'zogarh', 'meriem', 'meriem.zogarh@uit.ac.ma', 665290513, 17000418),
(2, 2, 'jdi', 'zainab', 'zainab.jdi@uit.ac.ma', 659856329, 17004340),
(3, 3, 'rougui', 'rania', 'rania.rougui@uit.ac.ma', 654132789, 17458654);

-- --------------------------------------------------------

--
-- Structure de la table `etudiant_has_module`
--

DROP TABLE IF EXISTS `etudiant_has_module`;
CREATE TABLE IF NOT EXISTS `etudiant_has_module` (
  `id_etudiant` int(11) NOT NULL,
  `id_module` int(11) NOT NULL,
  KEY `student_id` (`id_etudiant`),
  KEY `modul_id` (`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `etudiant_has_module`
--

INSERT INTO `etudiant_has_module` (`id_etudiant`, `id_module`) VALUES
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(2, 13),
(2, 14),
(2, 15),
(2, 16),
(2, 17);

-- --------------------------------------------------------

--
-- Structure de la table `filiere`
--

DROP TABLE IF EXISTS `filiere`;
CREATE TABLE IF NOT EXISTS `filiere` (
  `id_filiere` int(11) NOT NULL AUTO_INCREMENT,
  `nom_filiere` varchar(255) NOT NULL,
  PRIMARY KEY (`id_filiere`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `filiere`
--

INSERT INTO `filiere` (`id_filiere`, `nom_filiere`) VALUES
(1, 'science mathematiques informatiques (SMI)');

-- --------------------------------------------------------

--
-- Structure de la table `module`
--

DROP TABLE IF EXISTS `module`;
CREATE TABLE IF NOT EXISTS `module` (
  `id_module` int(11) NOT NULL AUTO_INCREMENT,
  `id_semestre` int(11) NOT NULL,
  `intitule_module` varchar(255) NOT NULL,
  PRIMARY KEY (`id_module`),
  KEY `semestre_id` (`id_semestre`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `module`
--

INSERT INTO `module` (`id_module`, `id_semestre`, `intitule_module`) VALUES
(1, 1, 'Analyse 2'),
(2, 1, 'Analyse 3'),
(3, 1, 'Algebre 3'),
(4, 1, 'Electricite'),
(5, 1, 'Optique'),
(6, 1, 'Algorithmique I'),
(7, 2, 'Architecture des ordinateurs\r\n'),
(8, 2, 'Programmation II'),
(9, 2, 'Systeme d\'exploitation II'),
(10, 2, 'Analyse Numerique'),
(11, 2, 'Structures de donnees'),
(12, 2, 'Electromagnetisme'),
(13, 3, 'Programmation Systeme'),
(14, 3, 'POO Java'),
(15, 3, 'Reseaux II'),
(16, 3, 'Base de donnees II'),
(17, 3, 'Projet tutore');

-- --------------------------------------------------------

--
-- Structure de la table `professeur`
--

DROP TABLE IF EXISTS `professeur`;
CREATE TABLE IF NOT EXISTS `professeur` (
  `id_professeur` int(11) NOT NULL AUTO_INCREMENT,
  `id_utilisateur` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telephone` int(10) NOT NULL,
  PRIMARY KEY (`id_professeur`),
  KEY `user_idd` (`id_utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `professeur`
--

INSERT INTO `professeur` (`id_professeur`, `id_utilisateur`, `nom`, `prenom`, `email`, `telephone`) VALUES
(3, 5, 'enneya', 'nourredine', 'enneya@uit.ac.ma', 654897485),
(4, 6, 'mbarki', 'samir', 'samir.mbarki@uit.ac.ma', 621141548),
(5, 4, 'souhar', 'abdelghani', 'abdelghani.souhar@uit.ac.ma', 620241291);

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

DROP TABLE IF EXISTS `salle`;
CREATE TABLE IF NOT EXISTS `salle` (
  `id_salle` int(11) NOT NULL AUTO_INCREMENT,
  `id_seance` int(11) NOT NULL,
  `num_salle` varchar(255) NOT NULL,
  PRIMARY KEY (`id_salle`),
  KEY `seance_id` (`id_seance`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `salle`
--

INSERT INTO `salle` (`id_salle`, `id_seance`, `num_salle`) VALUES
(1, 1, 'Salle C-1'),
(2, 2, 'AMPHI-4'),
(3, 3, 'AMPHI-2'),
(4, 4, 'AMPHI-2'),
(5, 5, 'AMPHI CED'),
(6, 7, 'Salle-21'),
(7, 8, 'Salle-22'),
(8, 9, 'Salle-23'),
(9, 10, 'Salle-22');

-- --------------------------------------------------------

--
-- Structure de la table `seance`
--

DROP TABLE IF EXISTS `seance`;
CREATE TABLE IF NOT EXISTS `seance` (
  `id_seance` int(11) NOT NULL AUTO_INCREMENT,
  `id_module` int(11) NOT NULL,
  `jour_seance` varchar(255) NOT NULL,
  `heure_debut` time NOT NULL,
  `heure_fin` time NOT NULL,
  `type_seance` varchar(255) NOT NULL,
  PRIMARY KEY (`id_seance`),
  KEY `module_id` (`id_module`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `seance`
--

INSERT INTO `seance` (`id_seance`, `id_module`, `jour_seance`, `heure_debut`, `heure_fin`, `type_seance`) VALUES
(1, 17, 'Lundi', '10:30:00', '11:45:00', 'Cours'),
(2, 17, 'Lundi', '12:00:00', '13:15:00', 'TP'),
(3, 15, 'Jeudi', '09:00:00', '10:15:00', 'Cours'),
(4, 13, 'Jeudi', '10:30:00', '11:45:00', 'Cours'),
(5, 14, 'Vendredi', '10:30:00', '11:45:00', 'Cours'),
(6, 16, 'Lundi', '13:30:00', '14:45:00', 'Cours'),
(7, 15, 'Mardi', '09:00:00', '10:15:00', 'TP'),
(8, 13, 'Mardi', '10:30:00', '11:45:00', 'TP'),
(9, 16, 'Mardi', '12:00:00', '13:15:00', 'TP'),
(10, 14, 'Vendredi', '12:00:00', '13:15:00', 'TP');

-- --------------------------------------------------------

--
-- Structure de la table `semestre`
--

DROP TABLE IF EXISTS `semestre`;
CREATE TABLE IF NOT EXISTS `semestre` (
  `id_semestre` int(11) NOT NULL AUTO_INCREMENT,
  `id_filiere` int(11) NOT NULL,
  `nom_semestre` varchar(255) NOT NULL,
  PRIMARY KEY (`id_semestre`),
  KEY `filiere_id` (`id_filiere`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `semestre`
--

INSERT INTO `semestre` (`id_semestre`, `id_filiere`, `nom_semestre`) VALUES
(1, 1, 'S2'),
(2, 1, 'S4'),
(3, 1, 'S6');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type_utilisateur` varchar(255) NOT NULL,
  PRIMARY KEY (`id_utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_utilisateur`, `login`, `password`, `type_utilisateur`) VALUES
(1, 'm.zogarh', 'E123456', 'etudiant'),
(2, 'z.jdi', 'E789456', 'etudiant'),
(3, 'r.rougui', 'E456123', 'etudiant'),
(4, 'a.souhar', 'P123456', 'professeur'),
(5, 'n.enneya', 'P789456', 'professeur'),
(6, 's.mbarki', 'P456123', 'professeur'),
(7, 'admin', 'admin', 'admin');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD CONSTRAINT `user_id` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`);

--
-- Contraintes pour la table `etudiant_has_module`
--
ALTER TABLE `etudiant_has_module`
  ADD CONSTRAINT `modul_id` FOREIGN KEY (`id_module`) REFERENCES `module` (`id_module`),
  ADD CONSTRAINT `student_id` FOREIGN KEY (`id_etudiant`) REFERENCES `etudiant` (`id_etudiant`);

--
-- Contraintes pour la table `module`
--
ALTER TABLE `module`
  ADD CONSTRAINT `semestre_id` FOREIGN KEY (`id_semestre`) REFERENCES `semestre` (`id_semestre`);

--
-- Contraintes pour la table `professeur`
--
ALTER TABLE `professeur`
  ADD CONSTRAINT `user_idd` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`);

--
-- Contraintes pour la table `salle`
--
ALTER TABLE `salle`
  ADD CONSTRAINT `seance_id` FOREIGN KEY (`id_seance`) REFERENCES `seance` (`id_seance`);

--
-- Contraintes pour la table `seance`
--
ALTER TABLE `seance`
  ADD CONSTRAINT `module_id` FOREIGN KEY (`id_module`) REFERENCES `module` (`id_module`);

--
-- Contraintes pour la table `semestre`
--
ALTER TABLE `semestre`
  ADD CONSTRAINT `filiere_id` FOREIGN KEY (`id_filiere`) REFERENCES `filiere` (`id_filiere`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
