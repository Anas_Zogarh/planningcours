<?php
include_once("../../ressources/myparam.inc.php");
include_once ("../../DAO/EtudiantDAO.php");
include_once ("../../DAO/Etudiant_has_moduleDAO.php");
$dsn="mysql:host=".MYHOST.";dbname=".BASE;
$user=MYUSER;
$pass=MYPASS;
$idcom = new PDO($dsn,$user,$pass);
if(!$idcom)
{
    echo "Erreur";
}

?>
<!DOCTYPE html>
<html lang="FR">
<head>
    <title>Planning des Cours</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../assets/css/uikit.min.css" />
    <script src="../../assets/js/uikit.min.js"></script>
    <script src="../../assets/js/uikit-icons.min.js"></script>
    <link rel="stylesheet" href="../../assets/css/etudiantStyle.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
</head>
<body>
    <div id="page-wrapper">
        <div class="uk-navbar-container" style="background-color: white;">
            <div class="uk-container uk-container-expand">
                <div class="uk-navbar">

                    <div class="uk-navbar-left">
                        <div class="uk-navbar-item">
                            <span class="uk-text-lead  uk-text-bolder"><img src="../../assets/img/Mon_Planning__3_-removebg-preview.png"></span>
                        </div>
                    </div>

                    <div class="uk-navbar-right">
                            <ul class="uk-navbar-nav">
                        <li class="uk-active"><a href="../../pages/student/index.php?idUser=<?php echo $_GET["idUser"] ?>" class="uk-text-bold"><span uk-icon="icon: file-edit" style="padding-right:10px"></span>Mes données</a></li>
                        <li class="uk-parent"><a href="../../pages/student/choixModule.php?idUser=<?php echo $_GET["idUser"] ?>" class="uk-text-bold"><span uk-icon="icon: calendar" style="padding-right:10px"></span>Mon Planning</a></li>
                        <li><a href="../../index.php" class="uk-text-bold"><span uk-icon="icon: sign-out" style="padding-right:10px"></span>Deconnexion</a></li>
                        <li><img src="../../assets/img/avatar_etudiant.jpg" alt="Avatar" class="avatar"></li>
                            </ul>
                    </div>

                </div>
            </div>
        </div>



    <!-- page contents -->
        <div class="uk-section uk-section-small">
            <div class="uk-container">
            
            <!--
                          Load student data
                      -->
            <?php
            $etudiantDAO = new EtudiantDAO($idcom);
            /**
             * var Etudiant $etudiant
             */
            $etudiant =  $etudiantDAO->getStudentData($_GET["idUser"]);

 

            ?>

            <form class="formulaire" action="../../actions/etudiant/updateData.php" method="POST">
                <input hidden name="idStudent" value="<?php echo $_GET["idUser"];?>">
                <h1>Informations pédagogiques</h1>
                    <div>
                    <div class="uk-inline uk-width-1-1">

                        <input placeholder="nom" name="nom" class="uk-input uk-form-large" type="text" value="<?php echo $etudiant->getNom(); ?>" disabled>
                    </div>
                </div>
                <div>
                    <div class="uk-inline uk-width-1-1">

                        <input value="<?php echo $etudiant->getPrenom(); ?>" disabled placeholder="prénom"  name="prenom" class="uk-input uk-form-large" >
                    </div>
                </div>
                <div>
                    <div class="uk-inline uk-width-1-1">

                        <input value="<?php echo $etudiant->getEmail(); ?>" placeholder="E-mail"  name="email" class="uk-input uk-form-large" >
                    </div>
                </div>
                <div>
                    <div class="uk-inline uk-width-1-1">

                        <input value="<?php echo $etudiant->getTelephone(); ?>"   placeholder="Téléphone"  name="phone" class="uk-input uk-form-large" >
                    </div>
                </div>
                <div>
                    <div class="uk-inline uk-width-1-1">

                        <input value="<?php echo $etudiant->getAppogee(); ?>" disabled  placeholder="Apogée"  name="apogee" class="uk-input uk-form-large" >
                    </div>
                </div>
                <div>
                    <button type="submit" class="uk-button uk-button-primary uk-button-large uk-width-1-1 submit">Mettre à jours</button>
                </div>

            </form>
        </div>
    <!--/page contents -->
    </div> 
<!--/div#page-wrapper-->
<div class="footer-clean">
<footer class="uk-margin-top container">
                <div class="row justify-content-center">
                    <div class="col-sm-4 col-md-3 item">
                        <h3>Mon Planning</h3>
                        <ul>
                            <li>Département d'informatique</li>
                            <li>Faculté des Sciences, B.P 133</li>
                            <li>Kénitra 14000</li>
                        </ul>
                    </div>
                    <div class="col-sm-4 col-md-3 item">
                        <h3>Liens Utiles</h3>
                        <ul>
                            <li><a href="https://www.uit.ac.ma/presentation-uit/">Université Ibn Tofail</a></li>
                            <li><a href="https://fs.uit.ac.ma/">Faculté des sciences</a></li>
                            <li><a href="http://ent.uit.ac.ma/ent/">Environement numérique de travail</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4 col-md-3 item">
                        <h3>Contact</h3>
                        <ul>
                            <li>Téléphone : (+212) 6 61 40 35 57</p></li>
                            <li>Email : presidence@uit.ac.ma</p></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 item social"><a href="#"><i class="icon ion-social-facebook"></i></a><a href="#"><i class="icon ion-social-twitter"></i></a><a href="#"><i class="icon ion-social-youtube"></i></a><a href="#"><i class="icon ion-social-instagram"></i></a>
                        <p class="copyright"> © Copyright <strong>Mon Planning</strong>. 2021</p>
                    </div>
             </div><!--/footer#page-footer-->
             </div> 

</footer>
</div>

</body>
</html>