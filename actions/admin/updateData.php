<?php
include_once("../../ressources/myparam.inc.php");
include_once ("../../DAO/AdministrateurDAO.php");
$dsn="mysql:host=".MYHOST.";dbname=".BASE;
$user=MYUSER;
$pass=MYPASS;

if(isset($_POST['email']) && isset($_POST['phone']) && isset($_POST['idUser']) ){
    /**
     * @var PDO $idcom
     */
    $idcom = new PDO($dsn,$user,$pass);
    $administrateurDAO = new AdministrateurDAO($idcom);
    $administrateurDAO->updateAdministrateur( $_POST['email'],  $_POST['phone'], $_POST['idUser']);


}
?>

<script>
    alert("Mise à jours réussie ! ");
    window.history.go(-1);
</script>