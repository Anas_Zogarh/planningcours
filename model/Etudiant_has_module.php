<?php

class Etudiant_has_module{
    private $id_etudiant_has_module;
    private $id_etudiant;
    private $id_module;

/**
     * Etudiant constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getIdEtudiant_has_module()
    {
        return $this->id_etudiant_has_module;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setIdEtudiant_has_module($id_etudiant_has_module)
    {
        $this->id_etudiant = $id_etudiant_has_module;
    }

    /**
     * @return mixed
     */
    public function getIdEtudiant()
    {
        return $this->id_etudiant;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setIdEtudiant($id_etudiant)
    {
        $this->id_etudiant = $id_etudiant;
    }

/**
     * @return mixed
     */
    public function getIdModule()
    {
        return $this->id_module;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setIdModule($id_module)
    {
        $this->id_module = $id_module;
    }

}