<?php
class Filiere
{
    private $id_filiere;
    private $nom_filiere;

    /**
     * Etudiant constructor.
     */
    public function __construct()
    {
    }

   
    public function getIdFiliere()
    {
        return $this->id_filiere;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setIdFiliere($id_filiere)
    {
        $this->id_filiere = $id_filiere;
    }

     /**
     * @return mixed
     */
    public function getNomFiliere()
    {
        return $this->nom_filiere;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setNomFiliere($nom_filiere)
    {
        $this->nom_filiere = $nom_filiere;
    }

}