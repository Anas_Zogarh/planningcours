
<!DOCTYPE html>
<html lang="FR">
    <head>
        <title>Planning des Cours !</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="assets/css/uikit.min.css" />
		<link rel="stylesheet" href="assets/css/style.css"/>
        <script src="assets/js/uikit.min.js"></script>
        <script src="assets/js/uikit-icons.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    </head>
    <body class="bod">
	<div class="uk-container uk-container-expand">
            <div class="uk-navbar">

                <div class="uk-navbar-left">
                    <div class="uk-navbar-item">
                        <span class="uk-text-lead  uk-text-bolder"><img src="assets/img/Mon_Planning__3_-removebg-preview.png"></span>
                    </div>
                </div>

                <div class="uk-navbar-right">
                    <ul class="uk-navbar-nav">
	                    <li class="uk-active"><a href="" class="uk-text-bold"><span uk-icon="icon: grid" style="padding-right:10px"></span>Présentation</a></li>
                        <li class="uk-parent"><a href="" class="uk-text-bold"><span uk-icon="icon: mail" style="padding-right:10px"></span>Contact</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>

	<div style=" position: absolute; width: 100%;height: 100%;">

	<div class=".uk-background-center-center">
        <?php
            if(isset($_GET['error'])){
                echo '<div  class="uk-alert-danger" uk-alert>';
                echo '<a class="uk-alert-close" uk-close></a>';
                echo '<p>Identifiant ou mot de passe non reconnu.</p>';
                echo '</div>';
            }
        ?>

		<div class="uk-container1">
			<div class="uk-grid-margin uk-grid uk-grid-stack" >
				<div class="uk-width-1-1@m">
					<div class="uk-margin uk-width-large uk-margin-auto uk-card uk-card-default uk-card-body uk-box-shadow-large">
						<h1 class="uk-card-title uk-text-center">Connectez-vous</h1>
						<form action="actions/user/connexion.php" method="POST">
							<div class="uk-margin">
								<div class="uk-inline uk-width-1-1">
									<span class="uk-form-icon" uk-icon="icon: mail"></span>
									<input name="login" class="uk-input uk-form-large" type="text">
								</div>
							</div>
							<div class="uk-margin">
								<div class="uk-inline uk-width-1-1">
									<span class="uk-form-icon" uk-icon="icon: lock"></span>
									<input name="password" class="uk-input uk-form-large" type="password">
								</div>
							</div>
							<div class="uk-margin">
								<button type="submit" class="uk-button uk-button-primary uk-button-large uk-width-1-1">Connexion</button>
							</div>
							<div class="uk-text-small uk-text-center">
								 <a href="#">Mot de passe oublié ?</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
    </body>
</html>