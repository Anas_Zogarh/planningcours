<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/model/Etudiant.php');

class EtudiantDAO
{
    /**
     * @var PDO
     */
    private $connexion;

    /**
     * EtudiantDAO constructor.
     * @param $connexion
     */
    public function __construct($connexion)
    {
        $this->connexion = $connexion;
    }


    public function getStudentData($idUser){
        $requete = "select * from etudiant where id_utilisateur ='" .$idUser . "'";

        $result=$this->connexion->query("$requete");

        if(!$result)
        {
            $mes_erreur=$this->connexion–>errorInfo();
            echo "Lecture impossible, code", $this->connexion->errorCode(),$mes_erreur[2];
        } else {
            $ligne = $result->fetchAll(PDO::FETCH_CLASS, "Etudiant");
            if(!empty($ligne)){
                /**
                 * @var Etudiant $etudiant
                 */
                $etudiant = $ligne[0];
                return $etudiant;

            }

        }

    }

    public function updateStudent($email, $phone, $idUser){


        $requete =  "update etudiant set  telephone = '" . $phone ."' ,   email = '" . $email ."' where id_utilisateur = '" . $idUser  ."'";

        $this->connexion->exec($requete);
    }
    public function getidUtilisateur()
    {
        $query =$this->connexion->query('SELECT MAX(id_utilisateur) AS maxval FROM utilisateur  ');
        $max_row = $query->fetch(PDO::FETCH_ASSOC);

       $max = $max_row['maxval'];
   
        return $max;
        }
       
    

    public function addStudent($idUtilisateur,$nom,$prenom,$email,$telephone,$apogee)
    {

      $q = "INSERT INTO etudiant(id_utilisateur,nom, prenom, email, telephone, Appogee) VALUES('" .$idUtilisateur. "','" .$nom. "','" .$prenom. "','" .$email. "','" .$telephone. "','" .$apogee. "')";
  
      $this->connexion->exec($q);
      

    }
    

    public function getFiliereEtudiant($idEtudiant){
        $requete = "select nom_filiere from filiere f, etudiant e where e.idFiliere = f.idFiliere and  e.	id_etudiant  = ".$idEtudiant;
        $result=$this->connexion->query($requete);
        return $result->fetch();
    }

    /**
     *  Getters and Setters
     */

    /**
     * @return mixed
     */
    public function getConnexion()
    {
        return $this->connexion;
    }

    /**
     * @param mixed $connexion
     */
    public function setConnexion($connexion)
    {
        $this->connexion = $connexion;
    }
    

}