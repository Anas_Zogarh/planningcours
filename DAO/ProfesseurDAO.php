<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/model/Professeur.php');

class ProfesseurDAO
{
    /**
     * @var PDO
     */
    private $connexion;

    /**
     * ProfesseurDAO constructor.
     * @param $connexion
     */
    public function __construct($connexion)
    {
        $this->connexion = $connexion;
    }

    public function getProfesseurData($idUser){
        $requete = "select * from professeur where id_utilisateur ='" .$idUser . "'";

        $result=$this->connexion->query("$requete");

        if(!$result)
        {
            $mes_erreur=$this->connexion–>errorInfo();
            echo "Lecture impossible, code", $this->connexion->errorCode(),$mes_erreur[2];
        } else {
            $ligne = $result->fetchAll(PDO::FETCH_CLASS, "Professeur");
            if(!empty($ligne)){
                /**
                 * @var Professeur $professeur
                 */
                $professeur = $ligne[0];
                return $professeur;

            }

        }

    }

    public function updateProfesseur($email, $phone, $idUser){


        $requete =  "update professeur set  telephone = '" . $phone ."' ,   email = '" . $email ."' where id_utilisateur = '" . $idUser  ."'";

        $this->connexion->exec($requete);

    }
    public function getidUtilisateur()
    {
        $query =$this->connexion->query('SELECT MAX(id_utilisateur) AS maxval FROM utilisateur  ');
        $max_row = $query->fetch(PDO::FETCH_ASSOC);

       $max = $max_row['maxval'];
   
        return $max;
        }
       
    

    public function addProf($idUtilisateur,$nom,$prenom,$email,$telephone)
    {

      $q = "INSERT INTO professeur(id_utilisateur,nom, prenom, email, telephone) VALUES('" .$idUtilisateur. "','" .$nom. "','" .$prenom. "','" .$email. "','" .$telephone. "')";
  
      $this->connexion->exec($q);
      

    }
    public function getFiliereProfesseur($idProfesseur){
    $req = "SELECT DISTINCT f.idFiliere, f.nom_filiere from professeur p , module m, semestre s, filiere f
where p.id_professeur=m.id_professeur
and m.id_semestre =s.id_semestre
and f.idFiliere =s.idFiliere
and p.id_professeur = " . $idProfesseur;

        $query =$this->connexion->query($req);
        return $query->fetchAll();

    }

    /**
     *  Getters and Setters
     */

    /**
     * @return mixed
     */
    public function getConnexion()
    {
        return $this->connexion;
    }

    /**
     * @param mixed $connexion
     */
    public function setConnexion($connexion)
    {
        $this->connexion = $connexion;
    }
}