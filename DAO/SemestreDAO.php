<?php
define('__ROOT1__', dirname(dirname(__FILE__)));
require_once(__ROOT1__.'/model/Semestre.php');

class SemestreDAO
{
    /**
     * @var PDO
     */
    private $connexion;

    /**
     * FiliereDAO constructor.
     * @param $connexion
     */
    public function __construct($connexion)
    {
        $this->connexion = $connexion;
    }

    public function getSemestreData($idSemestre){
        $requete = "select * from semestre where id_semestre ='" .$idSemestre . "'";

        $result=$this->connexion->query("$requete");

        if(!$result)
        {
            $mes_erreur=$this->connexion–>errorInfo();
            echo "Lecture impossible, code", $this->connexion->errorCode(),$mes_erreur[2];
        } else {
            $ligne = $result->fetchAll(PDO::FETCH_CLASS, "Semestre");
            if(!empty($ligne)){
                /**
                 * @var Semestre $semestre
                 */
                $semestre = $ligne[0];
                return $semestre;

            }

        }

    }
    public function idFiliere($nomF){
        $query =$this->connexion->query("SELECT id_filiere AS fil from filiere where nom_filiere='".$nomF."'");
        
        $max_row = $query->fetch(PDO::FETCH_ASSOC);

       $max = $max_row['fil'];
       return $max;
    }
  
    public function idsemestre($nom){
        $query =$this->connexion->query("SELECT id_semestre AS sem from semestre
        where nom_semestre='" .$nom . "' ");
        
        $result1= $query->fetch(PDO::FETCH_ASSOC);
        $idP = $result1['sem'];
        return $idP;
    }

    public function addSemestre($idFiliere,$nomSemestre){
        $q = "INSERT INTO semestre(id_filiere,nom_semestre) VALUES('" .$idFiliere. "','" .$nomSemestre. "')";
  
        $this->connexion->exec($q);
    }



    /**
     * @return mixed
     */
    public function getConnexion()
    {
        return $this->connexion;
    }

    /**
     * @param mixed $connexion
     */
    public function setConnexion($connexion)
    {
        $this->connexion = $connexion;
    }
}