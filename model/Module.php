<?php
class Module
{
    private $id_module;
    private $id_semestre;
    private $id_professeur;
    private $intitule_module;

    /**
     * Etudiant constructor.
     */
    public function __construct()
    {
    }
    /**
     * @return mixed
     */
    public function getIdModule()
    {
        return $this->id_module;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setIdModule($id_module)
    {
        $this->id_module = $id_module;
    }

    /**
     * @return mixed
     */
    public function getIdSemestre()
    {
        return $this->id_semestre;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setIdSemestre($id_semestre)
    {
        $this->id_semestre = $id_semestre;
    }

     /**
     * @return mixed
     */
    public function getIdProfesseur()
    {
        return $this->id_professeur;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setIdProfesseur($id_professeur)
    {
        $this->id_professeur = $id_professeur;
    }

     /**
     * @return mixed
     */
    public function getIntituleModule()
    {
        return $this->intitule_module;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setIntituleModule($intitule_module)
    {
        $this->intitule_module = $intitule_module;
    }

}