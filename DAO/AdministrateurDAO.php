<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/model/admin.php');

class AdministrateurDAO
{
    /**
     * @var PDO
     */
    private $connexion;

    /**
     * EtudiantDAO constructor.
     * @param $connexion
     */
    public function __construct($connexion)
    {
        $this->connexion = $connexion;
    }


    public function getAdministrateurData($idUser){
        $requete = "select * from administrateur where id_utilisateur ='" .$idUser . "'";

        $result=$this->connexion->query("$requete");

        if(!$result)
        {
            $mes_erreur=$this->connexion–>errorInfo();
            echo "Lecture impossible, code", $this->connexion->errorCode(),$mes_erreur[2];
        } else {
            $ligne = $result->fetchAll(PDO::FETCH_CLASS, "Administrateur");
            if(!empty($ligne)){
                /**
                 * @var Etudiant $etudiant
                 */
                $administrateur = $ligne[0];
                return $administrateur;

            }

        }

    }

    public function updateAdministrateur($email, $phone, $idUser){


        $requete =  "update administrateur set  telephone = '" . $phone ."' ,   email = '" . $email ."' where id_utilisateur = '" . $idUser  ."'";

        $this->connexion->exec($requete);
    }

    /**
     *  Getters and Setters
     */

    /**
     * @return mixed
     */
    public function getConnexion()
    {
        return $this->connexion;
    }

    /**
     * @param mixed $connexion
     */
    public function setConnexion($connexion)
    {
        $this->connexion = $connexion;
    }


}