<?php
include_once("../../ressources/myparam.inc.php");
include_once ("../../DAO/ModuleDAO.php");
$dsn="mysql:host=".MYHOST.";dbname=".BASE;
$user=MYUSER;
$pass=MYPASS;

if(isset($_POST['filiere']) ) {
    /**
     * @var PDO $idcom
     */
    $idcom = new PDO($dsn, $user, $pass);
    $moduleDao = new ModuleDAO($idcom);

    $moduleDao->getModuleByFiliere($_POST['filiere']);
}
