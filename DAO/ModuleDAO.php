<?php
define('__ROOTMODULEDAO__', dirname(dirname(__FILE__)));
require_once(__ROOTMODULEDAO__.'/model/Module.php');

class ModuleDAO
{
    /**
     * @var PDO
     */
    private $connexion;

    /**
     * FiliereDAO constructor.
     * @param $connexion
     */
    public function __construct($connexion)
    {
        $this->connexion = $connexion;
    }

    public function getModuleData($idModule){
        $requete = "select * from module where id_module ='" .$idModule . "'";

        $result=$this->connexion->query("$requete");

        if(!$result)
        {
            $mes_erreur=$this->connexion–>errorInfo();
            echo "Lecture impossible, code", $this->connexion->errorCode(),$mes_erreur[2];
        } else {
            $ligne = $result->fetchAll(PDO::FETCH_CLASS, "Module");
            if(!empty($ligne)){
                /**
                 * @var Module $module
                 */
                $module = $ligne[0];
                return $module;

            }

        }

    }
    public function getidProfesseur($nomP,$prenomP){
        
       
        $query =$this->connexion->query("SELECT id_professeur AS maxv from professeur 
        where nom ='" .$nomP. "' and prenom='" .$prenomP. "'");
        
        $result1= $query->fetch(PDO::FETCH_ASSOC);
        $idP = $result1['maxv'];
        return $idP;
    
          
        }
        public function getidSemestre($nomSemestre){
           
                $query =$this->connexion->query("SELECT id_semestre AS maxval from semestre where nom_semestre ='" .$nomSemestre. "'");
        
                $result2= $query->fetch(PDO::FETCH_ASSOC);
                $idS = $result2['maxval'];
   
           return $idS;
               
        
              
            }
           

  
    public function addModule($idsemestre,$idprof,$intitulemodule)
    {
      $q = "INSERT INTO module(id_semestre,id_professeur,intitule_module) VALUES('" .$idsemestre. "','" .$idprof. "','" .$intitulemodule. "')";
  
      $this->connexion->exec($q);

    }

    public function getModuleForStudent($idEtudiant){
        $requete = "select m.id_module, m.intitule_module from module m , etudiant e, semestre s, filiere f where  e.idFiliere = f.idfiliere and s.idfiliere = f.idfiliere and  s.id_semestre = m.id_semestre and e.id_etudiant =". $idEtudiant;
        $result=$this->connexion->query("$requete");
        return $result->fetchAll();
    }

    public function getModuleByFiliere($idFiliere){
        $requete = 'select m.id_module, m.intitule_module from module m , etudiant e, semestre s, filiere f where  e.idFiliere = f.idfiliere and s.idfiliere = f.idfiliere and  s.id_semestre = m.id_semestre' ;
        if($idFiliere != 0){
        $requete =$requete . ' and  f.idfiliere = ' . $idFiliere;

        }


        $result=$this->connexion->query($requete)->fetchAll(PDO::FETCH_ASSOC);

        $data[] =array();
        foreach ($result as $row) {
            $data[] = array(
                'id_module' => $row["id_module"],
                'intitule_module' => $row["intitule_module"]
            );
        }

        echo   json_encode($data);
    }
    
    /**
     * @return mixed
     */
    public function getConnexion()
    {
        return $this->connexion;
    }

    /**
     * @param mixed $connexion
     */
    public function setConnexion($connexion)
    {
        $this->connexion = $connexion;
    }
}