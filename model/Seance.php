<?php
class Seance
{
    private $id_seance;
    private $id_module;
    private $jour_seance;
    private $heure_debut;
    private $heure_fin;
    private $type_seance;
    

    /**
     * Etudiant constructor.
     */
    public function __construct()
    {
    }

        /**
     * @return mixed
     */
    public function getIdSeance()
    {
        return $this->id_seance;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setIdSeance($id_seance)
    {
        $this->id_seance = $id_seance;
    }

    /**
     * @return mixed
     */
    public function getIdModule()
    {
        return $this->id_module;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setIdModule($id_module)
    {
        $this->id_module = $id_module;
    }

    /**
     * @return mixed
     */
    public function getJourSeance()
    {
        return $this->jour_seance;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setJourSeance($jour_seance)
    {
        $this->jour_seance = $jour_seance;
    }

     /**
     * @return mixed
     */
    public function getHeureDebut()
    {
        return $this->heure_debut;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setHeureDebut($heure_debut)
    {
        $this->heure_debut = $heure_debut;
    }

     /**
     * @return mixed
     */
    public function getHeureFin()
    {
        return $this->heure_fin;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setHeureFin($heure_fin)
    {
        $this->heure_fin = $heure_fin;
    }

    /**
     * @return mixed
     */
    public function getTypeSeance()
    {
        return $this->type_seance;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setTypeSeance($type_seance)
    {
        $this->type_seance = $type_seance;
    }

}