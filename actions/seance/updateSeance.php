<?php
include_once("../../ressources/myparam.inc.php");
include_once ("../../DAO/SeanceDAO.php");
$dsn="mysql:host=".MYHOST.";dbname=".BASE;
$user=MYUSER;
$pass=MYPASS;

if(isset($_POST['jour']) && isset($_POST['heureDeb']) && isset($_POST['heureFin']) && isset($_POST['salle'])) {
    /**
     * @var PDO $idcom
     */
    $idcom = new PDO($dsn, $user, $pass);
    $seanceDao = new SeanceDAO($idcom);

    $seanceDao->updateSeance($_POST['jour'], $_POST['heureDeb'], $_POST['heureFin'], $_POST['salle'], $_POST['idSeance'] );
}
