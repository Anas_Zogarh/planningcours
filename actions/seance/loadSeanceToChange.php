<?php
include_once("../../ressources/myparam.inc.php");
include_once ("../../DAO/SeanceDAO.php");
$dsn="mysql:host=".MYHOST.";dbname=".BASE;
$user=MYUSER;
$pass=MYPASS;

if(isset($_POST['filiere']) && isset($_POST['modules']) ) {
    /**
     * @var PDO $idcom
     */
    $idcom = new PDO($dsn, $user, $pass);
    $seanceDao = new SeanceDAO($idcom);

    $seanceDao->loadSeanceToChange($_POST['filiere'], $_POST['modules']);
}
