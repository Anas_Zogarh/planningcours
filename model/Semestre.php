<?php
class Semestre
{
    private $id_semestre;
    private $id_filiere;
    private $nom_semestre;

    /**
     * Etudiant constructor.
     */
    public function __construct()
    {
    }

    
    /**
     * @return mixed
     */
    public function getIdSemestre()
    {
        return $this->id_semestre;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setIdSemestre($id_semestre)
    {
        $this->id_semestre = $id_semestre;
    }

     /**
     * @return mixed
     */
    public function getIdFiliere()
    {
        return $this->id_filiere;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setIdFiliere($id_filiere)
    {
        $this->id_filiere = $id_filiere;
    }

     /**
     * @return mixed
     */
    public function getNomSemestre()
    {
        return $this->nom_semestre;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setNomSemestre($nom_semestre)
    {
        $this->nom_semestre = $nom_semestre;
    }

}