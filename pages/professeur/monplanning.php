<?php
include_once("../../ressources/myparam.inc.php");
include_once ("../../DAO/ProfesseurDAO.php");

$dsn="mysql:host=".MYHOST.";dbname=".BASE;
$user=MYUSER;
$pass=MYPASS;
$idcom = new PDO($dsn,$user,$pass);
if(!$idcom)
{
    echo "Erreur";
}

?>
<!DOCTYPE html>
<html lang="FR">
<head>
    <title>Planning des Cours</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../assets/css/uikit.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
    <script src="../../assets/js/uikit.min.js"></script>
    <script src="../../assets/js/uikit-icons.min.js"></script>
    <link rel="stylesheet" href="../../assets/css/etudiantStyle.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <style>
        .row {
            width: 100%;
            display: flex;
            flex-direction: row;

        }
        .block {
            width: auto;
        }
    </style>

</head>
<body>
<div id="page-wrapper">
    <div class="uk-navbar-container" style="background-color: white;">
        <div class="uk-container uk-container-expand">
            <div class="uk-navbar">

                <div class="uk-navbar-left">
                    <div class="uk-navbar-item">
                        <span class="uk-text-lead  uk-text-bolder"><img src="../../assets/img/Mon_Planning__3_-removebg-preview.png"></span>
                    </div>
                </div>

                <div class="uk-navbar-right">
                    <ul class="uk-navbar-nav">
                        <li class="uk-parent"><a href="../../pages/professeur/index.php?idUser=<?php echo $_GET['idUser']?>" class="uk-text-bold"><span uk-icon="icon: file-edit" style="padding-right:10px"></span>Mes données</a></li>
                        <li class="uk-active"><a href="../../pages/professeur/monplanning.php?idUser=<?php echo $_GET["idUser"] ?>" class="uk-text-bold"><span uk-icon="icon: calendar" style="padding-right:10px"></span>Mon Planning</a></li>
                        <li><a href="../../index.php" class="uk-text-bold"><span uk-icon="icon: sign-out" style="padding-right:10px"></span>Deconnexion</a></li>
                        <li><img src="../../assets/img/avatar_etudiant.jpg" alt="Avatar" class="avatar"></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>


    <!-- page contents -->
    <div class="uk-section uk-section-small">
        <?php
        $profDAO = new ProfesseurDAO($idcom);
        /**
         * var Professeur $professeur
         */
        $professeur = $profDAO->getProfesseurData($_GET["idUser"]);

        $filiere = $profDAO->getFiliereProfesseur($professeur->getIdProfesseur());






        ?>

        <p class="uk-h2" style="margin-left: 30px"></p>

        <div class="uk-container">
            <div  class = "formulaire">
                <ul id="modules" uk-accordion>
                    <li class="uk-open">
                        <a   class="uk-accordion-title " href="#"><span class="uk-h3"> Filière :</span></a>
                        <div class="uk-accordion-content">
                            <div class="uk-width-medium-1-2 uk-container-center">


                                <div class="uk-column-1-2 uk-column-divider">
                                    <?php

                                    foreach ($filiere as $line){
                                        //print_r($line);
                                        echo "<lable style='display: block;padding-left: 15px;text-indent: -15px;'><input style='width: 13px;  height: 13px;  padding: 0;  margin:0;  vertical-align: bottom;  position: relative;  top: -4px;  *overflow: hidden;' name = \"$line[0]\" type=\"checkbox\" class=\"uk-checkbox\" /> $line[1]</lable>" ;


                                    }
                                    ?>
                                </div>
                                <br/>

                                <button   id="loadModulesPlanning" type="submit" class="uk-button uk-button-primary submit">Affichier Planning</button>
                                <button  id="uncheckAll" class="uk-button uk-button-secondary submit">Supprimer la selection</button>


                            </div>   </div>
                    </li>
                </ul>


            </div>
        </div>

        <div id="containerCalendar"  class="container">
            <div id="calendar"></div>
        </div>
    </div>
    <!--/page contents -->

    <!--/div#page-wrapper-->
    <div class="footer-clean">
        <footer class="uk-margin-top container">
            <div class="row justify-content-center">
                <div class="col-sm-4 col-md-3 item">
                    <h3>Mon Planning</h3>
                    <ul>
                        <li>Département d'informatique</li>
                        <li>Faculté des Sciences, B.P 133</li>
                        <li>Kénitra 14000</li>
                    </ul>
                </div>
                <div class="col-sm-4 col-md-3 item">
                    <h3>Liens Utiles</h3>
                    <ul>
                        <li><a href="https://www.uit.ac.ma/presentation-uit/">Université Ibn Tofail</a></li>
                        <li><a href="https://fs.uit.ac.ma/">Faculté des sciences</a></li>
                        <li><a href="http://ent.uit.ac.ma/ent/">Environement numérique de travail</a></li>
                    </ul>
                </div>
                <div class="col-sm-4 col-md-3 item">
                    <h3>Contact</h3>
                    <ul>
                        <li>Téléphone : (+212) 6 61 40 35 57</p></li>
                        <li>Email : presidence@uit.ac.ma</p></li>
                    </ul>
                </div>
                <div class="col-lg-3 item social"><a href="#"><i class="icon ion-social-facebook"></i></a><a href="#"><i class="icon ion-social-twitter"></i></a><a href="#"><i class="icon ion-social-youtube"></i></a><a href="#"><i class="icon ion-social-instagram"></i></a>
                    <p class="copyright"> © Copyright <strong>Mon Planning</strong>. 2021</p>
                </div>
            </div><!--/footer#page-footer-->
    </div>

    </footer>
</div>

</body>
<script>

    $(document).ready(function() {

        $("#uncheckAll").click(function (){
            $('input:checked').each(function() {
               $(this).prop( "checked", false );;
            });
        });

        $("#loadModulesPlanning").click(function (){
            $("#calendar").remove();
            $("#containerCalendar").html("<div id='calendar'></div>");
            var filiere="";
            UIkit.accordion($("#modules")).toggle(0, true);
            $('input:checked').each(function() {
                filiere+=(this.name+",");
            });

            var calendar = $('#calendar').fullCalendar({
                editable:false,
                firstDay: 1,

                //weekends: false,

                header:{
                    left:'prev,next today',
                    center:'title',


                    right:'agendaWeek,agendaDay'
                },
                events: '../../actions/seance/loadProfSeance.php?filiere=' + filiere.slice(0,-1) + '&idProf=' + <?php echo $professeur->getIdProfesseur() ?> ,
                selectable:false,
                selectHelper:false,

                editable:false,





            });
        });

    });

</script>
</html>