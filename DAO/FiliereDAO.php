<?php
define('__ROOT2__', dirname(dirname(__FILE__)));
require_once(__ROOT2__.'/model/Filiere.php');

class FiliereDAO
{
    /**
     * @var PDO
     */
    private $connexion;

    /**
     * FiliereDAO constructor.
     * @param $connexion
     */
    public function __construct($connexion)
    {
        $this->connexion = $connexion;
    }

    public function getFiliereData($idFiliere){
        $requete = "select * from filiere where id_filiere ='" .$idFiliere . "'";

        $result=$this->connexion->query("$requete");

        if(!$result)
        {
            $mes_erreur=$this->connexion–>errorInfo();
            echo "Lecture impossible, code", $this->connexion->errorCode(),$mes_erreur[2];
        } else {
            $ligne = $result->fetchAll(PDO::FETCH_CLASS, "Filiere");
            if(!empty($ligne)){
                /**
                 * @var Filiere $filiere
                 */
                $filiere = $ligne[0];
                return $filiere;

            }

        }

    }

    public function getAllFiliere(){
        $req = "select idfiliere, nom_filiere from filiere ";
        $result=$this->connexion->query($req);
        return $result->fetchAll();
    }
    /**
     * @return mixed
     */
    public function getConnexion()
    {
        return $this->connexion;
    }

    /**
     * @param mixed $connexion
     */
    public function setConnexion($connexion)
    {
        $this->connexion = $connexion;
    }
}