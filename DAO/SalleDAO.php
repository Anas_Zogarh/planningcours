<?php

require_once(dirname(dirname(__FILE__)).'/model/Salle.php');

class SalleDAO
{
    /** 
     * @var PDO
     */
    private $connexion;

    /**
     * FiliereDAO constructor.
     * @param $connexion
     */
    public function __construct($connexion)
    {
        $this->connexion = $connexion;
    }

    public function getAllSalle(){
        $requete = "select * from salle ";


        $lines=$this->connexion->query($requete)->fetchAll();
        return $lines;
    }
    public function getSalleData($idSalle){
        $requete = "select * from salle where id_salle ='" .$idSalle . "'";

        $result=$this->connexion->query("$requete");

        if(!$result)
        {
            $mes_erreur=$this->connexion–>errorInfo();
            echo "Lecture impossible, code", $this->connexion->errorCode(),$mes_erreur[2];
        } else {
            $ligne = $result->fetchAll(PDO::FETCH_CLASS, "Salle");
            if(!empty($ligne)){
                /**
                 * @var Salle $salle
                 */
                $salle = $ligne[0];
                return $salle;
            }

        }

    }


    /**
     * @return mixed
     */
    public function getConnexion()
    {
        return $this->connexion;
    }

    /**
     * @param mixed $connexion
     */
    public function setConnexion($connexion)
    {
        $this->connexion = $connexion;
    }
}