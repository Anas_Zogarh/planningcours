<?php


class User
{
    private $id_utilisateur;
    private $login;
    private $password;
    private $type_utilisateur;


    public function __construct()
    {

    }
    /**
     * User constructor.
     * @param $id_utilisateur
     * @param $login
     * @param $password
     * @param $type_utilisateur
     */
    public function buildUser($id_utilisateur, $login, $password, $type_utilisateur)
    {
        $this->id_utilisateur = $id_utilisateur;
        $this->login = $login;
        $this->password = $password;
        $this->type_utilisateur = $type_utilisateur;
    }

    /**
     *
     * Getters and setters
     *
     */

    /**
     * @return mixed
     */
    public function getIdUtilisateur()
    {
        return $this->id_utilisateur;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getTypeUtilisateur()
    {
        return $this->type_utilisateur;
    }

    /**
     * @param mixed $id_utilisateur
     */
    public function setIdUtilisateur($id_utilisateur)
    {
        $this->id_utilisateur = $id_utilisateur;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param mixed $type_utilisateur
     */
    public function setTypeUtilisateur($type_utilisateur)
    {
        $this->type_utilisateur = $type_utilisateur;
    }




}