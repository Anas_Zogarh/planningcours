<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
 
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../assets/css/uikit.min.css" />
    <script src="../../assets/js/uikit.min.js"></script>
    <script src="../../assets/js/uikit-icons.min.js"></script>
    <link rel="stylesheet" href="../../assets/css/adminStyle.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
</head>
 

  <title>Planning Cours</title>

  

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body>
<div class="uk-navbar-container" style="background-color: transparent;">
        <div class="uk-container uk-container-expand">
            <div class="uk-navbar">

                <div class="uk-navbar-left">
                    <div class="uk-navbar-item">
                        <span class="uk-text-lead  uk-text-bolder"><img src="../../assets/img/Mon_Planning__3_-removebg-preview.png"></span>
                    </div>
                </div>

                <div class="uk-navbar-right">
                    <ul class="uk-navbar-nav">
                        <li><a href="" class="uk-text-bold"><span uk-icon="icon: sign-out" style="padding-right:10px"></span>Deconnexion</a></li>
                        <li><img src="../../assets/img/avatar2.png" alt="Avatar" class="avatar"></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>

      
     
    <!--</div> leftpanelinner -->
  <!--</div> leftpanel -->
  
  <div class="mainpanel">
    
    <div class="headerbar">
      
      <a class="menutoggle"><i class="fa fa-bars"></i></a>
      
      <form class="searchform" action="http://themepixels.com/demo/webpage/bracket/index.html" method="post">
      </form><!-- header-right -->
      
    </div><!-- headerbar -->
    
    
       <div class="contentpanel">
      
      <div class="row">
        
        <div class="col-md-6">
          <form id="basicForm" action="../../actions/Module/addModule.php" method="post" class="form-horizontal">
          <div class="panel panel-default">
              <div class="panel-heading">
                <!--<div class="panel-btns">
                  <a href="#" class="panel-close">&times;</a>
                  <a href="#" class="minimize">&minus;</a>
                </div>-->
                <h4 class="panel-title">Ajouter un nouveau Module</h4>
              </div>
              
                
                <div class="panel-body">
                <div class="form-group">
                  <label class="col-sm-3 control-label">Module <span class="asterisk"></span></label>
                  <div class="col-sm-9">
                  <input type="text" name="module" class="form-control" placeholder="" required />
                  </div>
                </div>
                
                <div class="panel-body">
                <div class="form-group">
                  <label class="col-sm-3 control-label">Semestre <span class="asterisk"></span></label>
                  <div class="col-sm-9">
                    <input type="text" name="semestre" class="form-control" placeholder="" required />
                  </div>
                </div>
                  
				
               
                
                <div class="form-group">
                  <label class="col-sm-3 control-label">Filière<span class="asterisk">*</span></label>
                  <div class="col-sm-9">
                    <input type="text" name="filiere" class="form-control" placeholder="" required />
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label">Nom Professeur <span class="asterisk">*</span></label>
                  <div class="col-sm-9">
                    <input type="text" name="nomP" class="form-control" placeholder="" required />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Prenom Professeur <span class="asterisk">*</span></label>
                  <div class="col-sm-9">
                    <input type="text" name="prenomP" class="form-control" placeholder="" required />
                  </div>
                </div>    
                </div>           
              <div class="panel-footer">
                <div class="row">
                  <div class="col-sm-9 col-sm-offset-3">
                    <button class="btn btn-primary" type="submit">Ajouter</button>
                    <button type="reset" class="btn btn-default">Annuler</button>
                  </div>
                </div>
              </div>
            
          </div><!-- panel -->
          </form>
        </div><!-- col-md-6 -->
          
      </div><!--row -->
      
     
    

  
   
    
  
</section>


<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/jquery.sparkline.min.js"></script>
<script src="js/toggles.min.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/jquery.cookies.js"></script>

<script src="js/flot/flot.min.js"></script>
<script src="js/flot/flot.resize.min.js"></script>
<script src="js/morris.min.js"></script>
<script src="js/raphael-2.1.0.min.js"></script>

<script src="js/jquery.datatables.min.js"></script>
<script src="js/chosen.jquery.min.js"></script>

<script src="js/custom.js"></script>
<script src="js/dashboard.js"></script>


<script>
jQuery(document).ready(function(){
    
  // Chosen Select
  jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
  
  // Tags Input
  jQuery('#tags').tagsInput({width:'auto'});
   
  // Textarea Autogrow
  jQuery('#autoResizeTA').autogrow();
  
  // Color Picker
  if(jQuery('#colorpicker').length > 0) {
	 jQuery('#colorSelector').ColorPicker({
			onShow: function (colpkr) {
				jQuery(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				jQuery(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				jQuery('#colorSelector span').css('backgroundColor', '#' + hex);
				jQuery('#colorpicker').val('#'+hex);
			}
	 });
  }
  
  // Color Picker Flat Mode
	jQuery('#colorpickerholder').ColorPicker({
		flat: true,
		onChange: function (hsb, hex, rgb) {
			jQuery('#colorpicker3').val('#'+hex);
		}
	});
   
  // Date Picker
  jQuery('#datepicker').datepicker();
  
  jQuery('#datepicker-inline').datepicker();
  
  jQuery('#datepicker-multiple').datepicker({
    numberOfMonths: 3,
    showButtonPanel: true
  });
  
  // Spinner
  var spinner = jQuery('#spinner').spinner();
  spinner.spinner('value', 0);
  
  // Input Masks
  jQuery("#date").mask("99/99/9999");
  jQuery("#phone").mask("(999) 999-9999");
  jQuery("#ssn").mask("999-99-9999");
  
  // Time Picker
  jQuery('#timepicker').timepicker({defaultTIme: false});
  jQuery('#timepicker2').timepicker({showMeridian: false});
  jQuery('#timepicker3').timepicker({minuteStep: 15});

  
});
</script>
</div> 
<div class="footer-clean" style="  background-color: white;">
<footer class="uk-margin-top">
<div class="container">
                <div class="row justify-content-center">
                    <div class="col-sm-4 col-md-3 item">
                        <h3>Mon Planning</h3>
                        <ul>
                            <li>Département d'informatique</li>
                            <li>Faculté des Sciences, B.P 133</li>
                            <li>Kénitra 14000</li>
                        </ul>
                    </div>
                    <div class="col-sm-4 col-md-3 item">
                        <h3>Liens Utiles</h3>
                        <ul>
                            <li><a href="https://www.uit.ac.ma/presentation-uit/">Université Ibn Tofail</a></li>
                            <li><a href="https://fs.uit.ac.ma/">Faculté des sciences</a></li>
                            <li><a href="http://ent.uit.ac.ma/ent/">Environement numérique de travail</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4 col-md-3 item">
                        <h3>Contact</h3>
                        <ul>
                            <li>Téléphone : (+212) 6 61 40 35 57</p></li>
                            <li>Email : presidence@uit.ac.ma</p></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 item social"><a href="#"><i class="icon ion-social-facebook"></i></a><a href="#"><i class="icon ion-social-twitter"></i></a><a href="#"><i class="icon ion-social-youtube"></i></a><a href="#"><i class="icon ion-social-instagram"></i></a>
                        <p class="copyright"> © Copyright <strong>Mon Planning</strong>. 2021</p>
                    </div>
                </div>
            </div>
</footer><!--/footer#page-footer-->
    
    </div> 
</body>
</html>
