<?php
define('__ROOT1__', dirname(dirname(__FILE__)));
require_once(__ROOT1__.'/model/Etudiant_has_module.php');

class Etudiant_has_moduleDAO
{
    /**
     * @var PDO
     */
    private $connexion;

    /**
     * EtudiantDAO constructor.
     * @param $connexion
     */
    public function __construct($connexion)
    {
        $this->connexion = $connexion;
    }

    public function getEtudiantModuleData($idUser){
        
        $requete = "select intitule_module from module where id_module in 
        (select id_module from etudiant_has_module where id_etudiant in 
        (select id_etudiant from etudiant where id_utilisateur  ='" .$idUser . "'))";

        $result=$this->connexion->query("$requete");

        if(!$result)
        {
            $mes_erreur=$this->connexion–>errorInfo();
            echo "Lecture impossible, code", $this->connexion->errorCode(),$mes_erreur[2];
        } else {
            $ligne = $result->fetchAll(PDO::FETCH_CLASS, "Etudiant_has_module");
            if(!empty($ligne)){
                /**
                 * @var Etudiant $etudiant
                 */
                $etudiant_has_module = $ligne[0];
                return $etudiant_has_module;

            }

        }

    }
}