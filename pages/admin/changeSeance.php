<?php
include_once("../../ressources/myparam.inc.php");
include_once ("../../DAO/AdministrateurDAO.php");
include_once ("../../DAO/FiliereDAO.php");
include_once ("../../DAO/SalleDAO.php");
$dsn="mysql:host=".MYHOST.";dbname=".BASE;
$user=MYUSER;
$pass=MYPASS;
$idcom = new PDO($dsn,$user,$pass);
if(!$idcom)
{
    echo "Erreur";
}

?>
<!DOCTYPE html>
<html lang="FR">
<head>
    <title>Planning des Cours</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../assets/css/uikit.min.css" />
    <script src="../../assets/js/uikit.min.js"></script>
    <script src="../../assets/js/uikit-icons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <link rel="stylesheet" href="../../assets/css/adminStyle.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
</head>
<body>
<div id="page-wrapper">
    <div class="uk-navbar-container" style="background-color: white;">
        <div class="uk-container uk-container-expand">
            <div class="uk-navbar">

                <div class="uk-navbar-left">
                    <div class="uk-navbar-item">
                        <span class="uk-text-lead  uk-text-bolder"><img src="../../assets/img/Mon_Planning__3_-removebg-preview.png"></span>
                    </div>
                </div>

                <div class="uk-navbar-right">
                    <ul class="uk-navbar-nav">
                        <li class="uk-active"><a href="../../pages/admin/ajouterUtilisateur.php" class="uk-text-bold">Ajouter Utilisateur</a></li>
                        <li class="uk-parent"><a href="../../pages/admin/ajoutmodule.php" class="uk-text-bold">Ajouter Module</a></li>
                        <li><a href="" class="uk-text-bold">Deconnexion</a></li>
                        <li><img src="../../assets/img/avatar2.png" alt="Avatar" class="avatar"></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>

    <?php
    $filiereDAO = new FiliereDAO($idcom);

    $filiere = $filiereDAO->getAllFiliere();


    ?>


    <!-- page contents -->
    <div class="uk-section uk-section-small">
        <div class="uk-container">

            <div  class = "formulaire">
                <ul id="modules" uk-accordion>
                    <li class="uk-open">
                        <a   class="uk-accordion-title " href="#"><span class="uk-h3"> Critère de recherche :</span></a>
                        <div class="uk-accordion-content">
                            <div class="uk-width-medium-1-2 uk-container-center">


                                <div class="uk-column-1-2 uk-column-divider">
                                    <select  id="filieres"  style="width: 100%">
                                        <option value='0'> --- Toutes les filières --- </option>
                                        <?php

                                        foreach ($filiere as $line){
                                            echo "<option value='$line[0]'> $line[1] </option>";
                                        }

                                        ?>

                                    </select>
                                    <select  id="modulesSel" style="width: 100% ; margin-top: 20px" >
                                        <option value='0'> --- Tous les modules --- </option>

                                    </select>
                                </div>
                                <br/>

                                <button   id="search" type="submit" class="uk-button uk-button-primary submit">Générer planning</button>
                                <button  id="uncheckAll" class="uk-button uk-button-secondary submit">Vider la selection</button>

                                <table class="uk-table uk-table-responsive uk-table-divider">
                                    <thead>
                                    <tr>
                                        <th>Seance</th>
                                        <th>Jour</th>
                                        <th>Heure Début</th>
                                        <th>Heure Fin</th>
                                        <th>Salle</th>
                                        <th>Modifier</th>
                                    </tr>
                                    </thead>
                                    <tbody id="bodyResult">

                                    </tbody>
                                </table>
                            </div>   </div>
                    </li>
                </ul>


            </div>

        </div>

        <!--/page contents -->
    </div>
    <!--/div#page-wrapper-->
    <div class="footer-clean">
        <footer class="uk-margin-top">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-sm-4 col-md-3 item">
                        <h3>Mon Planning</h3>
                        <ul>
                            <li>Département d'informatique</li>
                            <li>Faculté des Sciences, B.P 133</li>
                            <li>Kénitra 14000</li>
                        </ul>
                    </div>
                    <div class="col-sm-4 col-md-3 item">
                        <h3>Liens Utiles</h3>
                        <ul>
                            <li><a href="https://www.uit.ac.ma/presentation-uit/">Université Ibn Tofail</a></li>
                            <li><a href="https://fs.uit.ac.ma/">Faculté des sciences</a></li>
                            <li><a href="http://ent.uit.ac.ma/ent/">Environement numérique de travail</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4 col-md-3 item">
                        <h3>Contact</h3>
                        <ul>
                            <li>Téléphone : (+212) 6 61 40 35 57</p></li>
                            <li>Email : presidence@uit.ac.ma</p></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 item social"><a href="#"><i class="icon ion-social-facebook"></i></a><a href="#"><i class="icon ion-social-twitter"></i></a><a href="#"><i class="icon ion-social-youtube"></i></a><a href="#"><i class="icon ion-social-instagram"></i></a>
                        <p class="copyright"> © Copyright <strong>Mon Planning</strong>. 2021</p>
                    </div>
                </div>
            </div>
    </div>

    </footer><!--/footer#page-footer-->
</div>
<div id="modal-sections" uk-modal>
    <div class="uk-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
            <h4 class="uk-modal-title" id="seanceTitle"></h4>
        </div>
        <div class="uk-modal-body">
            <input type="hidden" id="seanceToUpdate" value="" />
            <div class="uk-margin">
                <label class="uk-form-label" for="form-stacked-text">Jour</label>
                <div class="uk-form-controls">
                    <select class="uk-input" id="jourSeance" type="text" >
                        <option id="lundi">Lundi</option>
                        <option id="mardi">Mardi</option>
                        <option id="mercredi">Mercredi</option>
                        <option id="jeudi">Jeudi</option>
                        <option id="vendredi">Vendredi</option>
                        <option id="samedi">Samedi</option>
                    </select>
                </div>
            </div>

            <div class="uk-margin">
                <label class="uk-form-label" for="form-stacked-text">Heure Debut</label>
                <div class="uk-form-controls">
                    <input class="uk-input" id="heureDeb" type="text" >
                </div>
            </div>

            <div class="uk-margin">
                <label class="uk-form-label" for="form-stacked-text">Heure Fin</label>
                <div class="uk-form-controls">
                    <input class="uk-input" id="heureFin" type="text" >
                </div>
            </div>
            <div class="uk-margin">
                <label class="uk-form-label" for="form-stacked-text">Salle</label>
                <div class="uk-form-controls">
                    <select class="uk-input" id="listeSalle" type="text" >
                        <?php
                        $salleDao = new SalleDAO($idcom);
                        $result = $salleDao->getAllSalle();
                        foreach ($result as $row) {
                            echo '<option id="salOpt'.$row[0].'" name="seanceSalle'.$row[0].'" value="'.$row[0].'">'.$row[1].'</option>';
    }
                        ?>
                    </select>
                </div>
            </div>

        </div>

        <div class="uk-modal-footer uk-text-right">
            <button id="closeModale" class="uk-button uk-button-default uk-modal-close" type="button">Annuler</button>
            <button id="updateSeance" class="uk-button uk-button-primary" type="button">Enregistrer</button>
        </div>
    </div>

</div>
    <script>
        $(document).ready(function() {
            $("#updateSeance").click(function (){
                $.post("../../actions/seance/updateSeance.php", {
                    jour: $("#jourSeance").val(),
                    heureDeb: $("#heureDeb").val(),
                    heureFin: $("#heureFin").val(),
                    salle: $("#listeSalle").val(),
                    idSeance:$("#seanceToUpdate").val()
                }, function (data) {
                    $("#closeModale").click();
                    alert("Modification effectué.");
                    $("#search").click();


                });
            });


            $("#uncheckAll").click(function (){
                $("#filieres option").removeAttr("selected");
                $("#modulesSel option").removeAttr("selected");

                $("#filieres option[value='0']").attr("selected", "selected");
                $("#modulesSel option[value='0']").attr("selected", "selected");
            });

            $("#search").click(function () {



                $.post("../../actions/seance/loadSeanceToChange.php", {
                    filiere: $("#filieres").val(),
                    modules: $("#modulesSel").val()
                }, function (data) {
                    $("#bodyResult").empty();
                    var obj = JSON.parse(data);


                    for(var i in obj)
                    {
                            if(i > 0){
                                $("#bodyResult").append('<tr>');
                                $("#bodyResult").append('<td id=intitule'+obj[i].id_seance+ '>'+ obj[i].intitule_module+'</td>');
                                $("#bodyResult").append('<td id=jour'+obj[i].id_seance + '>'+ obj[i].jour_seance+'</td>');
                                $("#bodyResult").append('<td id=heurDeb'+obj[i].id_seance + '>'+ obj[i].heure_debut+'</td>');
                                $("#bodyResult").append('<td id=heurFin'+obj[i].id_seance + '>'+ obj[i].heure_fin+'</td>');
                                $("#bodyResult").append('<td idsalle='+obj[i].idSalle+' id=salle'+obj[i].id_seance + '>'+ obj[i].num_salle+'</td>');
                                $("#bodyResult").append('<td ><div  name="'+ obj[i].id_seance+'" id="changeSeanceLine'+ obj[i].id_seance+'"> <a style="margin-top: -3px" href="#modal-sections"  uk-toggle class="uk-icon-link "   uk-icon="file-edit"> </a></div></td>');

                                $("#bodyResult").append('</tr>');
                            }

                    }
                    $("[id*='changeSeanceLine']").click(function (){
                        var id = $(this).attr('name');

                        var titleSeance = $("#intitule" + id).text();
                        $("#seanceTitle").text("Modifier la Seance : " + titleSeance);
                        var jour = $("#jour" + id).text();
                        $("#jourSeance option[id*='i']").removeAttr("selected");
                        $("#jourSeance option[id='"+ jour.toLowerCase().trim() +"']").attr("selected", "selected");

                        var heurDeb = $("#heurDeb" + id).text()
                        $("#heureDeb").val(heurDeb);

                        var heurFin = $("#heurFin" + id).text()
                        $("#heureFin").val(heurFin);

                        var salle = $("#salle" + id).attr("idsalle");
                        //$("#listeSalle option[id*='salOpt']").removeAttr("selected");
                        $("#listeSalle").val(salle);
                        $("#listeSalle").change();
                        $("#seanceToUpdate").val(id);
                    });
                      /* 'intitule_module' => $row["intitule_module"],
                        'id_seance' => $row["id_seance"],
                        'heure_debut' => $row["heure_debut"],
                        'heure_fin' => $row["heure_fin"],
                        'jour_seance' => $row["jour_seance"],
                        'num_salle' => $row["num_salle"]*/
                });
            });

           $("#filieres").change(function (){


               $("#modulesSel").empty();
                   $.post("../../actions/Module/loadModuleByFiliere.php", { filiere :$("#filieres").val() }, function(data) {

                       var obj = JSON.parse(data);

                       $("#modulesSel").append('<option value="0"> --- Tous les modules --- </option>');
                       for(var i in obj)
                       {
                           if(i>0)
                                $("#modulesSel").append('<option value=' + obj[i].id_module + '>' + obj[i].intitule_module + '</option>');
                       }


           });

           })
        });
    </script>


</body>
</html>