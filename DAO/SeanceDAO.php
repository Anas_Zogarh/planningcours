<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/model/Seance.php');

class SeanceDAO
{
    /**
     * @var PDO
     */
    private $connexion;

    /**
     * FiliereDAO constructor.
     * @param $connexion
     */
    public function __construct($connexion)
    {
        $this->connexion = $connexion;
    }

    public function getSeanceData($idSeance){
        $requete = "select * from seance where id_seance ='" .$idSeance . "'";

        $result=$this->connexion->query("$requete");

        if(!$result)
        {
            $mes_erreur=$this->connexion–>errorInfo();
            echo "Lecture impossible, code", $this->connexion->errorCode(),$mes_erreur[2];
        } else {
            $ligne = $result->fetchAll(PDO::FETCH_CLASS, "Seance");
            if(!empty($ligne)){
                /**
                 * @var Seance $seance
                 */
                $seance = $ligne[0];
                return $seance;

            }

        }

    }



public function seance($modules){

    $data = array();



    $statement =$this->connexion->query("SELECT id_seance , heure_debut , heure_fin , jour_seance, intitule_module, sal.num_salle as num_salle 
    FROM module as m,seance as s,  salle as sal  
    where m.id_module=s.id_module
      and sal.id_salle = s.id_salle
    and   m.id_module IN (".$modules.") ".
    " ORDER BY id_seance");

    $result=$statement->fetchAll(PDO::FETCH_ASSOC);

    foreach($result as $row) {
        $dayOfWeek = 0;
        switch ($row["jour_seance"]){
            case "Lundi" :
                $dayOfWeek = 1;
                break;
            case "Mardi" :
                $dayOfWeek = 2;
                break;

            case "Mercredi" :
                $dayOfWeek = 3;
                break;

            case "Jeudi" :
                $dayOfWeek = 4;
                break;

            case "Vendredi" :
                $dayOfWeek = 5;
                break;

            case "Samedi" :
                $dayOfWeek = 6;
                break;
            default: break;
}

        $data[] = array(
            'id' => $row["id_seance"],
            'start' => $row["heure_debut"],
            'end' => $row["heure_fin"],
            'dow' => [$dayOfWeek ],
            'title' => $row["intitule_module"]. " (".$row["num_salle"] .")"

        );



}

echo json_encode($data);

}

public function loadSeanceProf($filiere, $idProf)
{
    $data = array();


    $statement = $this->connexion->query("select id_seance , heure_debut , heure_fin , jour_seance, intitule_module,sal.num_salle as num_salle  from module m , seance s, semestre sem, salle sal where 
m.id_module = s.id_module
and m.id_professeur = " . $idProf . " 
and m.id_semestre = sem.id_semestre
and sal.id_salle = s.id_salle 
and sem.idFiliere in (" . $filiere . ")  ORDER BY id_seance ");

    $result = $statement->fetchAll(PDO::FETCH_ASSOC);

    foreach ($result as $row) {
        $dayOfWeek = 0;
        switch ($row["jour_seance"]) {
            case "Lundi" :
                $dayOfWeek = 1;
                break;
            case "Mardi" :
                $dayOfWeek = 2;
                break;

            case "Mercredi" :
                $dayOfWeek = 3;
                break;

            case "Jeudi" :
                $dayOfWeek = 4;
                break;

            case "Vendredi" :
                $dayOfWeek = 5;
                break;

            case "Samedi" :
                $dayOfWeek = 6;
                break;
            default:
                break;
        }

        $data[] = array(
            'id' => $row["id_seance"],
            'start' => $row["heure_debut"],
            'end' => $row["heure_fin"],
            'dow' => [$dayOfWeek],
            'title' => $row["intitule_module"] . " (".$row["num_salle"] .")"


        );

    }
    echo json_encode($data);
}

public function loadSeanceToChange($filiere, $module){
        $req = 'SELECT intitule_module, id_seance , heure_debut , heure_fin , jour_seance,num_salle,sa.id_salle as idSalle   FROM seance s, module m, semestre sem, filiere f, salle sa
where
      
 m.id_module = s.id_module
and sa.id_salle = s.id_salle
and m.id_semestre=sem.id_semestre
and sem.idFiliere = f.idFiliere

';

        if($filiere > 0)
            $req = $req . ' and f.idFiliere = ' . $filiere ;
        if($module >0)
            $req = $req . ' and   s.id_module = '.$module;


    $result=$this->connexion->query($req)->fetchAll(PDO::FETCH_ASSOC);

    $data[] =array();
    foreach ($result as $row) {
        $data[] = array(
            'intitule_module' => $row["intitule_module"],
            'id_seance' => $row["id_seance"],
            'heure_debut' => $row["heure_debut"],
            'heure_fin' => $row["heure_fin"],
            'jour_seance' => $row["jour_seance"],
            'num_salle' => $row["num_salle"],
            'idSalle' => $row["idSalle"]

        );
    }

    echo   json_encode($data);
}
public function updateSeance($jour, $heureDeb, $heureFin, $salle, $idSeance){

        $jour = ucfirst($jour);
      $req = "update seance set jour_seance= '" . $jour . "'".
           ", heure_debut= '" . $heureDeb . "'".
           ", heure_fin= '" . $heureFin . "'".
           ", id_salle='" . $salle . "'".
           " where id_seance = '" . $idSeance ."'";


    $this->connexion->exec($req);


}

public function insertSeance(){

if(isset($_POST["title"]))
{

 $statement  =$this->connexion->query( "INSERT INTO seance (heure_debut,heure_fin, type_seance) 
 VALUES (:start_event, :end_event, :title)
 ");
 $statement->execute(
  array(
   
   ':start_event' => $start,
   ':end_event' => $end,
   ':title'  => $title,
  )
 );
}
}


    /**
     * @return mixed
     */
    public function getConnexion()
    {
        return $this->connexion;
    }

    /**
     * @param mixed $connexion
     */
    public function setConnexion($connexion)
    {
        $this->connexion = $connexion;
    }
}