<?php
class Seance
{
    private $id_salle;
    private $id_seance;
    private $num_salle;
    

    /**
     * Etudiant constructor.
     */
    public function __construct()
    {
    }

            /**
     * @return mixed
     */
    public function getIdSalle()
    {
        return $this->id_salle;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setIdSalle($id_salle)
    {
        $this->id_salle = $id_salle;
    }


        /**
     * @return mixed
     */
    public function getIdSeance()
    {
        return $this->id_seance;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setIdSeance($id_seance)
    {
        $this->id_seance = $id_seance;
    }

    /**
     * @return mixed
     */
    public function getNumSalle()
    {
        return $this->num_salle;
    }

    /**
     * @param mixed $id_etudiant
     */
    public function setNumSalle($num_salle)
    {
        $this->num_salle = $num_salle;
    }

}