<?php
define('__ROOT4__', dirname(dirname(__FILE__)));
require_once(__ROOT4__.'/model/User.php');


class UserDao
{
    /**
     * @var PDO
     */
    private $connexion;

    /**
     * UserDao constructor.
     * @param $connexion
     */
    public function __construct($connexion)
    {
        $this->connexion = $connexion;
    }

    public function verifyCredentials($login , $password ){

        $requete="SELECT * FROM utilisateur where login = '" . $login . "' and password = '" .$password  ."'";

        $result=$this->connexion->query($requete);
        if(!$result)
        {
            $mes_erreur=$this->connexion–>errorInfo();
            echo "Lecture impossible, code", $this->connexion->errorCode(),$mes_erreur[2];
        } else {
            $ligne= $result->fetchAll(PDO::FETCH_CLASS, "User");
            /** @var User $user */
            if(!empty($ligne)){
                $user = $ligne[0];
                $_SESSION['username'] = $user->getLogin();

                switch ($user->getTypeUtilisateur()) {
                    case 'etudiant':

                        return $_SERVER['HTTP_HOST'] .'/PlanningCours/pages/student/index.php?idUser='.$user->getIdUtilisateur();
                    case 'professeur':
                        return $_SERVER['HTTP_HOST'] .'/PlanningCours/pages/professeur/index.php?idUser='.$user->getIdUtilisateur();
                    case 'admin':
                        return $_SERVER['HTTP_HOST'] .'/PlanningCours/pages/admin/index.php?idUser='.$user->getIdUtilisateur();
                }
            } else {
                return $_SERVER['HTTP_HOST'] .'/PlanningCours/index.php?error=1';
            }
        }

    }
    /*public function logout(){
        Session::delete($user->getLogin());
    }
    public function addUser($login,$password,$tutilisateur)
    {
      $q = "INSERT INTO utilisateur(login,password,type_utilisateur) VALUES('" .$login. "','" .$password. "','" .$tutilisateur. "')";
  
      $this->connexion->exec($q);

    }*/
    

/**
     * @return mixed
     */
    public function getConnexion()
    {
        return $this->connexion;
    }

    /**
     * @param mixed $connexion
     */
    public function setConnexion($connexion)
    {
        $this->connexion = $connexion;
    }
}